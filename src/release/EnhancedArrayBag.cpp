/**
 * CSC232 Data Structures with C++
 * Missouri State University, Spring 2017.
 *
 * @file    EnhancedArrayBag.cpp
 * @authors Dillon Flohr <dillon987@live.missouristate.edu>
 *          Jake Wakefield <wakefield515@live.missouristate.edu>
 * @brief   Implementation file for an Enhanced Array Bag
 *
 */
#include "EnhancedArrayBag.h"

template<class ItemType>
EnhancedArrayBag<ItemType> EnhancedArrayBag<ItemType>::unionWithBag(const EnhancedArrayBag<ItemType>& other) const {
	
	EnhancedArrayBag<ItemType> unionBag;
	
	std::vector<ItemType> bag1 = this -> toVector();
	std::vector<ItemType> bag2 = other.toVector();
	for (ItemType element : bag1) {
		unionBag.add(element);
	}
	for (ItemType element : bag2) {
		unionBag.add(element);
	}
	return unionBag;
}

template<class ItemType>
EnhancedArrayBag<ItemType> EnhancedArrayBag<ItemType>::intersectionWithBag(const EnhancedArrayBag<ItemType>& other) const {
	
	EnhancedArrayBag<ItemType> intersectBag;
	
	std::vector<ItemType> bag1 = this -> toVector();
	std::vector<ItemType> bag2 = other.toVector();
	for (ItemType element : bag1) {
		if (other.contains(element)) {
			intersectBag.add(element);
		}
	}
	return intersectBag;
}

template<class ItemType>
EnhancedArrayBag<ItemType> EnhancedArrayBag<ItemType>::differenceWithBag(const EnhancedArrayBag<ItemType>& other) const {
	
	EnhancedArrayBag<ItemType> differenceBag;
	
	std::vector<ItemType> bag1 = this -> toVector();
	std::vector<ItemType> bag2 = other.toVector();
	for(ItemType element : bag1) {
		if (other.getFrequencyOf(element) == 0) {
			differenceBag.add(element);
		}
	}
	return differenceBag;
}