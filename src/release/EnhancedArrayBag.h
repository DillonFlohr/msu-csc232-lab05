/**
 * CSC232 Data Structures with C++
 * Missouri State University, Spring 2017.
 *
 * @file    EnhancedArrayBag.h
 * @authors Dillon Flohr <dillon987@live.missouristate.edu>
 *          Jake Wakefield <wakefield515@live.missouristate.edu>
 * @brief   Header file for an Enhanced Array Bag
 *
 */
#ifndef ENHANCED_ARRAY_BAG_
#define ENHANCED_ARRAY_BAG_

#include "ArrayBag.h"

template<class ItemType>
class EnhancedArrayBag : public ArrayBag<ItemType> {
	
public:

	/** 
    * Make a union set of two bags.
	*
    * @param other Bag to perform the union with.
    * @return An EnhancedArrayBag that is the union of this bag and other.
    */
	EnhancedArrayBag<ItemType> unionWithBag(const EnhancedArrayBag<ItemType>& other) const;
	/** 
    * Make a intersection set of two bags.
	*
    * @param other Bag to perform the intersection with.
    * @return An EnhancedArrayBag that is the intersection of this bag and other.
    */
	EnhancedArrayBag<ItemType> intersectionWithBag(const EnhancedArrayBag<ItemType>& other) const;
	/** 
    * Make a difference set of two bags.
	*
    * @param other Bag to perform the difference with.
    * @return An EnhancedArrayBag that is the difference of this bag and other.
    */
	EnhancedArrayBag<ItemType> differenceWithBag(const EnhancedArrayBag<ItemType>& other) const;
};

#include "EnhancedArrayBag.cpp"
#endif